Main Instructions




Installation

To use the shared library version of GLEW, you need to copy the headers and libraries into their destination directories. On Windows this typically boils down to copying:
bin/glew32.dll	    to    	%SystemRoot%/system32
lib/glew32.lib	    to    	{VC Root}/Lib
include/GL/glew.h	    to    	{VC Root}/Include/GL
include/GL/wglew.h	    to    	{VC Root}/Include/GL
where {VC Root} is the Visual C++ root directory, typically C:/Program Files/Microsoft Visual Studio/VC98 for Visual Studio 6.0 or C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK for Visual Studio .NET.
On Unix, typing make install will attempt to install GLEW into /usr/include/GL and /usr/lib. You can customize the installation target via the GLEW_DEST environment variable if you do not have write access to these directories.