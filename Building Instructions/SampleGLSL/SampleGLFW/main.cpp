#include "GLFW\glfw3.h"
#include <GL/glut.h>

#include <stdio.h>

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	double time = -1;
	double actualTime = -1;
	char title[128];

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		if (time == -1)
		{
			time = glfwGetTime();
		}
		else
		{
			actualTime = glfwGetTime() - time;
			time = glfwGetTime();
		}
		sprintf_s(title, "Hello World. Time: %1.5f & FPS: %1.5f & F: %1.5f", actualTime, 1.0f / actualTime, 1.0f / 60);
		glfwSetWindowTitle(window, title);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}