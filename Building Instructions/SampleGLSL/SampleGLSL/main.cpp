#include<stdio.h>
#define GLEW_STATIC

#include <GL/glew.h>
#include <GL/glut.h>
//<gl, glu, and glut functionality is available here>

void main(int argcp, char** argv)
{
	/*
	void glutInit(int *argcp, char **argv);

	argcp

	A pointer to the program's unmodified argc variable from main. 
	Upon return, the value pointed to by argcp will be updated, 
	because glutInit extracts any command line options intended 
	for the GLUT library.

	argv

	The program's unmodified argv variable from main. Like argcp, 
	the data for argv will be updated because glutInit extracts any 
	command line options understood by the GLUT library.
	*/
	glutInit(&argcp, argv);

	glutCreateWindow("GLEW Test");

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		//Problem: glewInit failed, something is seriously wrong.
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
}